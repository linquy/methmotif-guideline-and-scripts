according to IDR results, select TFs passing the quality check.

Identified_TF_list_template.txt is file containing a list of TFs. The most important column is the first TF column and the following two columns are not important.

Based on MEME-ChIP results, later added the following columns 1) if there is highly and centrally enriched motif, and 2) the i-th fimo folder containing the scanned TFBSs or meme motif to be re-scanned by fimo.