import subprocess

tf_table = open("Identified_TF_list_template.txt","r")
log = open("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/motif_analysis_by_MEME_Log.txt","w")

for tf_line in tf_table:
	tf = (tf_line.strip("\n").split("\t"))[0]

	#mkdir 
	run_mkdir = "mkdir /root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	#fetch sequence
	print("START fetching sequence 200 bp in "+tf)
	inputbed = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/Peak_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peak_summit_no_blacklist.bed"
	outputfasta = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/"+tf+"_optimal_peak_summit_no_blacklist_200bp.fasta"

	run_fetch_seq = "fetch-sequences  -v 1 -genome hg38 -downstr_ext 100 -upstr_ext 100 -i "+inputbed+" -o "+outputfasta
	log.write(run_fetch_seq+"\n\n")
	print(run_fetch_seq)
	fetch_seq = subprocess.Popen(run_fetch_seq, shell=True)
	fetch_seq.communicate()
	print("Successfully fetched sequence 200 bp in "+tf)

	# meme
	print("START motif analysis by MEME-CHIP in "+tf)
	meme_outputdir = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/meme_result"
	meme_inputfasta = outputfasta
	run_meme = "meme-chip -o "+meme_outputdir+" -dna "+meme_inputfasta
	print(run_meme)
	log.write(run_meme+"\n\n")
	meme = subprocess.Popen(run_meme, shell=True)
	meme.communicate()

tf_table.close()
log.close()





