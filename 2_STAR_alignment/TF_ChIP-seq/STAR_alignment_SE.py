import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/TFs_STAR_alignment_SE_log_1.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	sub_db = line_db[1].split(",")
	judge_db = line_db[3].split(",")

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	for i in range(len(sub_db)):
		sub = sub_db[i]
		judge = judge_db[i]
		if judge == "YES":
			inputbam = sub+".fastq.gz"
		else:
			inputbam = sub+"_trimmed.fastq.gz"

		run_star = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 15 --limitBAMsortRAM 11000000000 --readFilesIn /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/"+tf+"/"+inputbam+" --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+"_"
		print(run_star)
		log.write(run_star+"\n\n")
		star = subprocess.Popen(run_star, shell=True)
		star.communicate()

ref.close()
log.close()



