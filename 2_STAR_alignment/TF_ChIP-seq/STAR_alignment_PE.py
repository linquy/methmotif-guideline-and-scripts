import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/TFs_STAR_alignment_log_PE.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep1_db = line_db[1].split(",")
	rep2_db = line_db[2].split(",")

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf
	mkdir = subprocess.Popen(run_mkdir,shell=True)
	mkdir.communicate()

	#rep1
	run_STAR1 = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 30 --limitBAMsortRAM 11000000000 --readFilesIn /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/"+tf+"/rep1/"+rep1_db[0]+".fastq.gz /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/"+tf+"/rep1/"+rep1_db[1]+".fastq.gz --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep1_"
	print(run_STAR1)
	log.write(run_STAR1+"\n")
	STAR1 = subprocess.Popen(run_STAR1, shell=True)
	STAR1.communicate()

	#rep2
	run_STAR2 = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 30 --limitBAMsortRAM 11000000000 --readFilesIn /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/"+tf+"/rep2/"+rep2_db[0]+".fastq.gz /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/"+tf+"/rep2/"+rep2_db[1]+".fastq.gz --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep2_"
	print(run_STAR2)
	log.write(run_STAR2+"\n\n")
	STAR2 = subprocess.Popen(run_STAR2, shell=True)
	STAR2.communicate()

ref.close()
log.close()





