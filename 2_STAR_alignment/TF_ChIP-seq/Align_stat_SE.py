ref = open("TFs_with_their_replicates_SE_template.txt", "r")
out = open("STAR_align_results_SE_stat.txt","w")

for line in ref:
	line_db = line.strip().split("\t")
	tf = line_db[0]
	sub_db = line_db[1].split(",")
	lineout = tf
	for i in range(len(sub_db)):
		stat = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+"_Log.final.out", "r")
		for sen in stat:
			if sen.find("Uniquely mapped reads number")>=0:
				out1 = (sen.strip("\n").split("\t"))[1]
			if sen.find("Uniquely mapped reads %")>=0:
				out2 = (sen.strip("\n").split("\t"))[1]
		stat.close()
		lineout = lineout+"\t"+out1+"("+out2+")"
	out.write(lineout+"\n")

ref.close()
out.close()