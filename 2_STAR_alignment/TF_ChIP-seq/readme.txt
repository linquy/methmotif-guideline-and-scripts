PE - paired end
SE - single end

TFs_with_their_replicates_SE_template.txt is separated by tab and columns represent TF, ENCODE accessions for ChIPseq replicate, phred (based on FASTQC results), and judegement if ChIPseq quality is good or not (based on FASTQC results).

TFs_with_their_replicates_PE_template.txt is separated by tab and columns represent TF, ENCODE accessions for two ChIPseq read ends in replicate one, ENCODE accessions for two ChIPseq read ends in replicate two, phred (based on FASTQC results), and judegement if ChIPseq quality is good or not (based on FASTQC results) in each replicate.

Alignment:
command line: python3 STAR_alignment_SE.py
command line: python3 STAR_alignment_PE.py

After alignmenet, get read alignment percentage:
command line: python3 Align_stat_SE.py
command line: python3 Align_stat_PE.py

