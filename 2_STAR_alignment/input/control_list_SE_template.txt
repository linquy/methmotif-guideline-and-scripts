ENCSR000AKY	ENCFF000BWK,ENCFF977WHL,ENCFF885VML,ENCFF994FIB,ENCFF402EPP,ENCFF222TAO,ENCFF156ECZ,ENCFF199NQH,ENCFF561WFK,ENCFF185XBW,ENCFF283HQV	-phred33,-phred33,-phred33,-phred33,-phred33,-phred33,-phred33,-phred33,-phred33,-phred33,-phred33	NO,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES
ENCSR000BGG	ENCFF000QET,ENCFF000QEU,ENCFF000QFL,ENCFF000QFS	-phred33,-phred33,-phred64,-phred33	NO,NO,NO,YES
ENCSR000BLJ	ENCFF000QEX,ENCFF000QEZ	-phred64,-phred64	NO,YES
ENCSR000BRJ	ENCFF000QFG,ENCFF000QFI,ENCFF000QFJ,ENCFF000QFK	-phred33,-phred33,-phred33,-phred33	NO,YES,NO,YES
ENCSR000EHI	ENCFF000YRB	-phred64	NO
ENCSR000EHM	ENCFF000YQB,ENCFF000YQC	-phred64,-phred64	YES,YES
ENCSR000EWK	ENCFF000VEK	-phred64	YES
ENCSR000FAK	ENCFF000YRC,ENCFF000YRN	-phred64,-phred64	YES,YES
ENCSR000FCA	ENCFF001RVO	-phred33	YES
ENCSR080EOT	ENCFF002ECS,ENCFF002ECW	-phred33,-phred33	YES,YES
ENCSR129NAJ	ENCFF774EJH	-phred33	YES
ENCSR264VDP	ENCFF993VKM	-phred33	YES
ENCSR436OZQ	ENCFF089EYX,ENCFF419XJY	-phred33,-phred33	YES,YES
ENCSR440JAU	ENCFF317LUD,ENCFF338QTL	-phred33,-phred33	YES,YES
ENCSR457PLC	ENCFF911KYO,ENCFF113ZAT	-phred33,-phred33	YES,YES
ENCSR483GYO	ENCFF521UDO,ENCFF343MYT	-phred33,-phred33	YES,YES
ENCSR599SPN	ENCFF002BGC	-phred33	YES
ENCSR700IEB	ENCFF913JVQ	-phred33	YES
ENCSR824WND	ENCFF001RVP	-phred33	YES
ENCSR830KKO	ENCFF001RVN	-phred33	YES
ENCSR896JFO	ENCFF001RVQ	-phred33	YES
ENCSR932KYH	ENCFF510UAH,ENCFF092DDZ	-phred33,-phred33	YES,YES
ENCSR962APM	ENCFF400XMH	-phred33	YES
ENCSR966DOU	ENCFF201ZVH,ENCFF025UQF	-phred33,-phred33	YES,YES
ENCSR996HUG	ENCFF002BGB	-phred33	YES
