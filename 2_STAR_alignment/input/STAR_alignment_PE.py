import subprocess

ref = open("control_list_PE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/STAR_alignment_PE_log.txt", "w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	file = line_db[0]
	rep1_db = line_db[1].split(",")
	rep2_db = line_db[2].split(",")

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	inputfile1 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep1_db[0]+".fastq.gz  /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep1_db[1]+".fastq.gz"
	outdir1 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep1_db[0]+"_"+rep1_db[1]+"_"
	run_star1 = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 20 --limitBAMsortRAM 11000000000 --readFilesIn "+inputfile1+" --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix "+outdir1
	print(run_star1)
	log.write(run_star1+"\n")
	star1 = subprocess.Popen(run_star1, shell=True)
	star1.communicate()

	inputfile2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep2_db[0]+".fastq.gz  /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep2_db[1]+".fastq.gz"
	outdir2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep2_db[0]+"_"+rep2_db[1]+"_"
	run_star2 = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 20 --limitBAMsortRAM 11000000000 --readFilesIn "+inputfile2+" --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix "+outdir2
	print(run_star2)
	log.write(run_star2+"\n\n")
	star2 = subprocess.Popen(run_star2, shell=True)
	star2.communicate()

ref.close()
log.close()
