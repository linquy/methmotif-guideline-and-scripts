import subprocess

ref = open("control_list_SE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/STAR_alignment_SE_log.txt", "w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	file = line_db[0]
	rep_db = line_db[1].split(",")
	judge_db = line_db[3].split(",")

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	for i in range(len(rep_db)):
		rep = rep_db[i]
		judge = judge_db[i]
		if judge == "NO":
			inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+file+"/"+rep+"_trimmed.fastq.gz"
		else:
			inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+file+"/"+rep+".fastq.gz"
		outdir = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep+"_"

		run_star = "STAR --genomeDir ~/STAR_genome_ref_for_DNAseq_chromosomeOnly_no_ChrM_hg38/ --readFilesCommand zcat --genomeLoad LoadAndKeep --runThreadN 30 --limitBAMsortRAM 11000000000 --readFilesIn "+inputfile+" --alignIntronMax 1 --alignEndsType EndToEnd --outFilterMultimapNmax 1 --outFilterMatchNminOverLread 0.80 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix "+outdir
		print(run_star)
		log.write(run_star+"\n\n")
		star = subprocess.Popen(run_star, shell=True)
		star.communicate()

ref.close()
log.close()
