import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self/self_idr_analysis_SE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep_db = line_db[1].split(",")
	if len(rep_db) == 1:
		continue
	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	input1 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self/"+tf+"/"+tf+"_rep1_vs_control_peaks_top100k.narrowPeak"
	input2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self/"+tf+"/"+tf+"_rep2_vs_control_peaks_top100k.narrowPeak"
	output = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self/"+tf+"/"+tf+"_rep1_vs_rep2"
	run_idr = "Rscript batch-consistency-analysis.r "+input1+" "+input2+" -1 "+output+" 0 F p.value"
	print(run_idr)
	log.write(run_idr+"\n\n")
	idr = subprocess.Popen(run_idr, shell=True)
	idr.communicate()

ref.close()
log.close()
