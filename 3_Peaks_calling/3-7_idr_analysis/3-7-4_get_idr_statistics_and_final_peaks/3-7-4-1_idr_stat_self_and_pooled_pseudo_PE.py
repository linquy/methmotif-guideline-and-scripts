import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt","r")
out = open("idr_analysis_self_and_pooled_pseudo_PE.txt","w")
out.write("tf\tself\tpooled_pseudo\n")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]

	#self 
	inputfile_self = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self/"+tf+"/"+tf+"_rep1_vs_rep2-overlapped-peaks.txt"
	run_self = "awk '$11 < 0.05 {print}' "+inputfile_self+" | wc -l"
	print(run_self)
	count_self = subprocess.Popen(run_self, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_self,err_self = count_self.communicate()
	out_self = (out_self.decode("utf-8")).strip("\n").strip()
	print(out_self)

	#pooled_pseudo
	inputfile_pooled = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/pooled_pseudo/"+tf+"/"+tf+"_pooled_rep1_vs_rep2-overlapped-peaks.txt"
	run_pooled = "awk '$11 < 0.01 {print}' "+inputfile_pooled+" | wc -l"
	print(run_pooled)
	count_pooled = subprocess.Popen(run_pooled, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_pooled,err_pooled = count_pooled.communicate()
	out_pooled = (out_pooled.decode("utf-8")).strip("\n").strip()
	print(out_pooled)

	out.write(tf+"\t"+str(out_self)+"\t"+str(out_pooled)+"\n")
ref.close()
out.close()
	