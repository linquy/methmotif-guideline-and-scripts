import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt","r")
out = open("idr_analysis_self_pseudo_SE.txt","w")
out.write("tf\tself_pseudo\n")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep_db = line_db[1].split(",")
	if len(rep_db) ==1:
		continue

	#self 
	inputfile_self1 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self_pseudo/"+tf+"/"+tf+"_rep1_pr1_vs_pr2-overlapped-peaks.txt"
	run_self1 = "awk '$11 < 0.05 {print}' "+inputfile_self1+" | wc -l"
	print(run_self1)
	count_self1 = subprocess.Popen(run_self1, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_self1,err_self1 = count_self1.communicate()
	out_self1 = (out_self1.decode("utf-8")).strip("\n").strip()
	print(out_self1)

	inputfile_self2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/self_pseudo/"+tf+"/"+tf+"_rep2_pr1_vs_pr2-overlapped-peaks.txt"
	run_self2 = "awk '$11 < 0.05 {print}' "+inputfile_self2+" | wc -l"
	print(run_self2)
	count_self2 = subprocess.Popen(run_self2, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_self2,err_self2 = count_self2.communicate()
	out_self2 = (out_self2.decode("utf-8")).strip("\n").strip()
	print(out_self2)

	out.write(tf+"\t"+str(out_self1)+","+str(out_self2)+"\n")
ref.close()
out.close()
	