import subprocess

ref = open("idr_analysis_self_and_pooled_pseudo_PE.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/final_set_PE_log.txt","w")

x = next(ref)
for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	self_num = int(line_db[1])
	pooled_num = int(line_db[2])
	final_num = max(self_num, pooled_num)

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	
	inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/pooled/"+tf+"/"+tf+"_pooled_vs_control_peaks.narrowPeak"
	outputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peaks.narrowPeak"

	run_form = "cat "+inputfile+" | sort -k8nr,8nr | head -n "+str(final_num)+" > "+outputfile
	print(run_form)
	log.write(run_form+"\n")
	form = subprocess.Popen(run_form, shell=True)
	form.communicate()
	
	inputfile2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/pooled/"+tf+"/"+tf+"_pooled_vs_control_summits.bed"
	outputfile2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peak_summit.bed"

	run_form2 = "cat "+inputfile2+" | sort -k5nr,5nr | head -n "+str(final_num)+" > "+outputfile2
	print(run_form2)
	log.write(run_form2+"\n")
	form2 = subprocess.Popen(run_form2, shell=True)
	form2.communicate()

	
	#rm blacklist
	outputfile_noblack = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peaks_no_blacklist.narrowPeak"
	input_black = "/home/quy/blacklist/ENCODE/ENCFF419RSJ.bed"
	run_rmblack = "intersectBed -a "+outputfile+" -b "+input_black+" -v > "+outputfile_noblack
	print(run_rmblack)
	log.write(run_rmblack+"\n\n")
	rmblack = subprocess.Popen(run_rmblack, shell=True)
	rmblack.communicate()
	

ref.close()
log.close()

