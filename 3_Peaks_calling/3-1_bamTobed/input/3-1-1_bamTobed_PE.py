import subprocess

ref = open("control_list_PE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/bamTobed_PE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	file = line_db[0]
	
	for i in range(2):
		rep_sub = line_db[i+1].split(",")
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep_sub[0]+"_"+rep_sub[1]+"_Aligned.sortedByCoord.out.bam"
		sorted_bam = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep_sub[0]+"_"+rep_sub[1]+"_Aligned.sortedByName.out.bam"
		run_sort = "samtools sort -n -o "+sorted_bam+" -@ 10 "+inputfile
		print(run_sort)
		log.write(run_sort+"\n")
		sort = subprocess.Popen(run_sort, shell=True)
		sort.communicate()

		outfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/rep"+str(i+1)+".tagAlign.gz"
		run_bamtobed = "samtools view -b -F 1548 "+sorted_bam+" | bamToBed -i stdin -bedpe | awk 'BEGIN{FS=\"\t\";OFS=\"\t\"}{$7=\"N\"; print $0}' | gzip -c > "+outfile
		print(run_bamtobed)
		log.write(run_bamtobed+"\n")
		bamtobed = subprocess.check_call(["/bin/bash", "-c", run_bamtobed])

		run_rm = "rm "+sorted_bam
		print(run_rm)
		log.write(run_rm+"\n\n")
		rm = subprocess.Popen(run_rm, shell=True)
		rm.communicate()

ref.close()
log.close()

