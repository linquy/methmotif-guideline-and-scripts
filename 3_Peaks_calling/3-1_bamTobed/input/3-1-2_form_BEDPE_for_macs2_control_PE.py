import subprocess

ref = open("control_list_PE_uniq.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/form_BEDPE_for_macs2_control_PE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	for i in range(2):
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+tf+"/rep"+str(i+1)+".tagAlign.gz"
		outfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+tf+"/rep"+str(i+1)+"_for_macs2.tagAlign.gz"
		run_bedpe = "zcat "+inputfile+" | cut -f 1,2,6 | gzip -c >> "+outfile
		print(run_bedpe)
		log.write(run_bedpe+"\n")
		bedpe = subprocess.check_call(["/bin/bash", "-c", run_bedpe])


	inputfile_1 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+tf+"/rep1_for_macs2.tagAlign.gz"
	inputfile_2 = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+tf+"/rep2_for_macs2.tagAlign.gz"
	outfile_merge = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+tf+"/"+tf+"_pooled_for_macs2.tagAlign.gz"
	run_merge = "zcat "+inputfile_1+" "+inputfile_2+" | gzip -c > "+outfile_merge
	print(run_merge)
	log.write(run_merge+"\n\n")
	merge = subprocess.check_call(["/bin/bash", "-c", run_merge])

ref.close()
log.close()

