import subprocess

ref = open("control_list_SE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/bamTobed_SE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	file = line_db[0]
	rep_db = line_db[1].split(",")
	inputfile_bed = ""
	for rep in rep_db:
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep+"_Aligned.sortedByCoord.out.bam"
		outfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+rep+".tagAlign.gz"
		run_bamtobed = "samtools view -b -F 1548 "+inputfile+" | bamToBed -i stdin | awk 'BEGIN{FS=\"\t\";OFS=\"\t\"}{$4=\"N\"; print $0}' | gzip -c > "+outfile
		print(run_bamtobed)
		log.write(run_bamtobed+"\n")
		bamtobed = subprocess.check_call(["/bin/bash", "-c", run_bamtobed])
		inputfile_bed = inputfile_bed+outfile+" "

	run_merge = "zcat "+inputfile_bed+" | gzip -c > /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+file+"/"+file+"_pooled.tagAlign.gz"
	print(run_merge)
	log.write(run_merge+"\n\n")
	merge = subprocess.check_call(["/bin/bash", "-c", run_merge])
ref.close()
log.close()
