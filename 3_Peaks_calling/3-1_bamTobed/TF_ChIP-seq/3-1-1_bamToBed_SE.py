import subprocess
ref = open("TFs_with_their_replicates_SE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/bamToBed_SE_log.txt", "w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep_db = line_db[1].split(",")
	for num in range(len(rep_db)):
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(num+1)+"_Aligned.sortedByCoord.out.bam"
		outfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(num+1)+".tagAlign.gz"
		run_bamtobed = "samtools view -b -F 1548 "+inputfile+" | bamToBed -i stdin | awk 'BEGIN{FS=\"\t\";OFS=\"\t\"}{$4=\"N\"; print $0}' | gzip -c > "+outfile
		print(run_bamtobed)
		log.write(run_bamtobed+"\n\n")
		bamtobed = subprocess.check_call(["/bin/bash", "-c", run_bamtobed])
ref.close()
log.close()