import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/GM12878_methylation_profiles_at_TF_peaks/transcription_factors_added_on_15JUN2017/STAR_alignment/form_BEDPE_for_macs2_in_PE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]

	for i in range(2):
	
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+".tagAlign.gz"
		output = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+"_for_macs2.tagAlign.gz"

		run_form = "zcat "+inputfile+" | cut -f 1,2,6 | gzip -c  >> "+output
		print(run_form)
		log.write(run_form+"\n\n")
		form = subprocess.check_call(["/bin/bash", "-c", run_form])

ref.close()
log.close()


