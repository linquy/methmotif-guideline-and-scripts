import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/pooled_pseudo/form_top100k_peaks_for_idr_PE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	for i in range(2):
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/pooled_pseudo/"+tf+"/"+tf+"_pooled_pr"+str(i+1)+"_vs_control_peaks.narrowPeak"
		outfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/pooled_pseudo/"+tf+"/"+tf+"_pooled_pr"+str(i+1)+"_vs_control_peaks_top100k.narrowPeak"
		run_truncate = "sort -k 8nr,8nr "+inputfile+" | head -n 100000 > "+outfile
		print(run_truncate)
		log.write(run_truncate+"\n\n")
		truncate = subprocess.Popen(run_truncate, shell=True)
		truncate.communicate()

ref.close()
log.close()