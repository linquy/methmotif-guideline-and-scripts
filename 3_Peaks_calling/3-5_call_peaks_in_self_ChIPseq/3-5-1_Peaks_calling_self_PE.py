import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self/self_peaks_calling_log_PE.txt", "w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	control = line_db[5]

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	inputcontrol = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+control+"/"+control+"_pooled_for_macs2.tagAlign.gz"
	for i in range(2):
		inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+"_for_macs2.tagAlign.gz"
		run_macs2 = "macs2 callpeak -t "+inputfile+" -c "+inputcontrol+" --outdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self/"+tf+"/ -f BEDPE -n "+tf+"_rep"+str(i+1)+"_vs_control --keep-dup auto -g hs -p 1e-2"
		print(run_macs2)
		log.write(run_macs2+"\n\n")
		macs2 = subprocess.Popen(run_macs2, shell=True)
		macs2.communicate()
ref.close()
log.close()



