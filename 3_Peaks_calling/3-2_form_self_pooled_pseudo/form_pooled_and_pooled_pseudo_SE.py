import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt","r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/Form_pooled_and_pooled_pseudo_SE_log.txt","w")

for line in ref:
	line_db =line.strip("\n").split("\t")
	tf = line_db[0]
	rep_db = line_db[1].split(",")
	inputdir = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"
	#pooled
	inputfile = ""
	for i in range(len(rep_db)):
		inputfile = inputfile+inputdir+tf+"_rep"+str(i+1)+".tagAlign.gz "
	run_pool = "zcat "+inputfile+" | gzip -c > "+inputdir+tf+"_pooled.tagAlign.gz"

	print(run_pool)
	log.write(run_pool+"\n")
	pool = subprocess.Popen(run_pool, shell=True)
	pool.communicate()

	run_count = "zcat "+inputdir+tf+"_pooled.tagAlign.gz | wc -l"
	print(run_count)
	log.write(run_count+"\n")
	count = subprocess.Popen(run_count, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out,err = count.communicate()
	out = (out.decode("utf-8")).strip("\n").strip()
	print(out)
	log.write(str(out)+"\n")

	#split
	out_half = str(((int(out))+1)//2)
	run_split = "zcat "+inputdir+tf+"_pooled.tagAlign.gz | shuf | split -d -l "+out_half+" - "+inputdir+tf
	print(run_split)
	log.write(run_split+"\n")
	split = subprocess.Popen(run_split, shell=True)
	split.communicate()

	run_compress1 = "gzip "+inputdir+tf+"00"
	print(run_compress1)
	log.write(run_compress1+"\n")
	compress1 = subprocess.Popen(run_compress1, shell=True)
	compress1.communicate()

	run_compress2 = "gzip "+inputdir+tf+"01"
	print(run_compress2)
	log.write(run_compress2+"\n")
	compress2 = subprocess.Popen(run_compress2, shell=True)
	compress2.communicate()

	run_mv1 = "mv "+inputdir+tf+"00.gz "+inputdir+tf+"_pooled.pr1.tagAlign.gz"
	print(run_mv1)
	log.write(run_mv1+"\n")
	mv1 = subprocess.Popen(run_mv1, shell=True)
	mv1.communicate()

	run_mv2 = "mv "+inputdir+tf+"01.gz "+inputdir+tf+"_pooled.pr2.tagAlign.gz"
	print(run_mv2)
	log.write(run_mv2+"\n")
	mv2 = subprocess.Popen(run_mv2, shell=True)
	mv2.communicate()

ref.close()
log.close()







