ARID1B	ENCFF718FBA,ENCFF521KBW	ENCFF680HKS,ENCFF065YSO	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR632PAG
ARNT	ENCFF546YEQ,ENCFF446XHP	ENCFF841ZHV,ENCFF382GYA	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ATF2	ENCFF043SEF,ENCFF780EID	ENCFF307XRL,ENCFF951ZXP	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ATF7	ENCFF002DOR,ENCFF002EIJ	ENCFF002DTY,ENCFF002DOV	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
BCLAF1	ENCFF744DBX,ENCFF834VSH	ENCFF188VON,ENCFF574FTC	-phred33,-phred33	YES,YES	ENCSR173USI
BCOR	ENCFF423KKF,ENCFF047XMV	ENCFF949LLJ,ENCFF036YYM	-phred33,-phred33	YES,YES	ENCSR173USI
BRCA1	ENCFF276RGL,ENCFF231TGV	ENCFF576EVC,ENCFF805AVF	-phred33,-phred33	YES,YES	ENCSR632PAG
C11orf30	ENCFF076YGT,ENCFF380PYR	ENCFF749RRK,ENCFF325DTB	-phred33,-phred33	YES,YES	ENCSR632PAG
CDC5L	ENCFF847ZNJ,ENCFF300KRT	ENCFF899BAM,ENCFF146CPM	-phred33,-phred33	YES,YES	ENCSR173USI
CHAMP1	ENCFF740JJX,ENCFF507REW	ENCFF088KVK,ENCFF576YEJ	-phred33,-phred33	YES,YES	ENCSR632PAG
COPS2	ENCFF257UIV,ENCFF042QKO	ENCFF610FSQ,ENCFF064WKY	-phred33,-phred33	YES,YES	ENCSR632PAG
CREB3L1	ENCFF002EIN,ENCFF002EIS	ENCFF002EIT,ENCFF002EIQ	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
CTBP1	ENCFF034WZZ,ENCFF350AMO	ENCFF829YQD,ENCFF077DIB	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
DDX20	ENCFF771DFR,ENCFF931EDX	ENCFF881HGO,ENCFF071FDG	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
DNMT1	ENCFF436CBC,ENCFF306GVI	ENCFF126MZP,ENCFF553GOC	-phred33,-phred33	YES,YES	ENCSR173USI
DPF2	ENCFF579BJG,ENCFF031JCO	ENCFF389HJO,ENCFF290HFY	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
E2F1	ENCFF907JIE,ENCFF399CCC	ENCFF749JUG,ENCFF942QFN	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
E2F7	ENCFF124KEV,ENCFF584BPF	ENCFF928ZAS,ENCFF971CJJ	-phred33,-phred33	YES,YES	ENCSR658AGP
EGR1	ENCFF871VWH,ENCFF045QIG	ENCFF025AGM,ENCFF751XCN	-phred33,-phred33	YES,YES	ENCSR658AGP
ELF1	ENCFF932JXG,ENCFF070OBA	ENCFF132GWN,ENCFF456MJL	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ELF4	ENCFF270PKY,ENCFF029HOW	ENCFF587AYW,ENCFF625HJX	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ESRRA	ENCFF002DOX,ENCFF002EFV	ENCFF002DUB,ENCFF002DOY	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
FOXA1	ENCFF394CJB,ENCFF186GBM	ENCFF692TZI,ENCFF762YIW	-phred33,-phred33	YES,YES	ENCSR658AGP
FOXK2	ENCFF260CTF,ENCFF959CFI	ENCFF344DMA,ENCFF164JCA	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
FOXM1	ENCFF167VUS,ENCFF007LTF	ENCFF888ZYM,ENCFF749GVI	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
GATAD2B	ENCFF552ETP,ENCFF523ULM	ENCFF489DPL,ENCFF999EFL	-phred33,-phred33	YES,YES	ENCSR632PAG
HDAC1	ENCFF895UGJ,ENCFF910QTP	ENCFF677ULQ,ENCFF410PAM	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR632PAG
HDAC2	ENCFF362ZRF,ENCFF896IZU	ENCFF990UUV,ENCFF741IHV	-phred33,-phred33	YES,YES	ENCSR173USI
HDGF	ENCFF598OBQ,ENCFF324VNE	ENCFF664YJD,ENCFF214MWL	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
HES1	ENCFF429KAX,ENCFF887TVH	ENCFF583IRH,ENCFF600EDO	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
IKZF1	ENCFF015LUT,ENCFF424OOE	ENCFF687HHR,ENCFF222FXU	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
KAT8	ENCFF005NCE,ENCFF932SKI	ENCFF505YGU,ENCFF093OLE	-phred33,-phred33	YES,YES	ENCSR173USI
KDM1A	ENCFF147CXN,ENCFF408SWF	ENCFF263QAS,ENCFF763NMB	-phred33,-phred33	YES,YES	ENCSR632PAG
L3MBTL2	ENCFF945QMW,ENCFF139CLF	ENCFF756MPL,ENCFF347VNR	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
LEF1	ENCFF643QTB,ENCFF103BRX	ENCFF211HML,ENCFF238ZLM	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
MCM3	ENCFF568RGZ,ENCFF567PIT	ENCFF721WRK,ENCFF501RUA	-phred33,-phred33	YES,YES	ENCSR173USI
MCM5	ENCFF383AYS,ENCFF710PPD	ENCFF447WZH,ENCFF238TRM	-phred33,-phred33	YES,YES	ENCSR173USI
MCM7	ENCFF250AVG,ENCFF121ACO	ENCFF640FTH,ENCFF909ZLD	-phred33,-phred33	YES,YES	ENCSR173USI
MEIS2	ENCFF002EIU,ENCFF002EIV	ENCFF002EIW,ENCFF002EIX	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
MGA	ENCFF434ISC,ENCFF105BAY	ENCFF194JCS,ENCFF831HED	-phred33,-phred33	YES,YES	ENCSR632PAG
MIER1	ENCFF391PZV,ENCFF060OGJ	ENCFF910KPS,ENCFF612DVW	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
MLLT1	ENCFF994RSC,ENCFF755BUR	ENCFF135KOW,ENCFF789UDV	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
MNT	ENCFF678PWC,ENCFF627YOB	ENCFF911ICF,ENCFF393ZLC	-phred33,-phred33	YES,YES	ENCSR632PAG
MTA1	ENCFF638OLJ,ENCFF032QDB	ENCFF434PLO,ENCFF139SOU	-phred33,-phred33	YES,YES	ENCSR173USI
MTA2	ENCFF693CFR,ENCFF238FNW	ENCFF693GDA,ENCFF259AYK	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
MTA3	ENCFF002EFW,ENCFF002DPG	ENCFF894NUI,ENCFF809IJU	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
NBN	ENCFF321LCC,ENCFF581MDG	ENCFF854CCC,ENCFF881JDL	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
NCOA1	ENCFF165NJF,ENCFF226TLO	ENCFF137DWP,ENCFF033TZD	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
NCOR1	ENCFF443EYG,ENCFF435JEO	ENCFF224PWB,ENCFF337XLT	-phred33,-phred33	YES,YES	ENCSR632PAG
NFIC	ENCFF439DZE,ENCFF456ASW	ENCFF534ZOY,ENCFF077ONG	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR632PAG
NFRKB	ENCFF340VHI,ENCFF710XSY	ENCFF705WPL,ENCFF139VNK	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
NFXL1	ENCFF277SWU,ENCFF197EBM	ENCFF358LFL,ENCFF575GFV	-phred33,-phred33	YES,YES	ENCSR173USI
NONO	ENCFF100NWH,ENCFF594FPL	ENCFF902KOT,ENCFF499MPN	-phred33,-phred33	YES,YES	ENCSR632PAG
NR2C1	ENCFF146EKI,ENCFF102DIC	ENCFF025RHU,ENCFF562VET	-phred33,-phred33	YES,YES	ENCSR632PAG
NR2F1	ENCFF394OKZ,ENCFF579DKO	ENCFF691DSF,ENCFF810SJL	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
NRF1	ENCFF002DPA,ENCFF002EFX	ENCFF002EFZ,ENCFF002DPF	-phred33,-phred33	YES,YES	ENCSR173USI
PHB2	ENCFF672NIZ,ENCFF513NXC	ENCFF499ADG,ENCFF005PFF	-phred33,-phred33	YES,YES	ENCSR173USI
PKNOX1	ENCFF755HHM,ENCFF292YNE	ENCFF921VOK,ENCFF871SUI	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
RAD51	ENCFF354UBV,ENCFF600KXI	ENCFF273REO,ENCFF809ISJ	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
RB1	ENCFF615SOF,ENCFF656UEH	ENCFF935GDU,ENCFF268JOY	-phred33,-phred33	NO,NO	ENCSR658AGP
REST	ENCFF002EJC,ENCFF002EIZ	ENCFF002EJE,ENCFF002EJH	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
RFX1	ENCFF031QKW,ENCFF473RLY	ENCFF493EZE,ENCFF207YQO	-phred33,-phred33	YES,YES	ENCSR632PAG
RNF2	ENCFF470MPB,ENCFF742JMT	ENCFF446JLZ,ENCFF411FCT	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR632PAG
RUNX1	ENCFF002DOZ,ENCFF002EGE	ENCFF002EGD,ENCFF002DPH	-phred33,-phred33	YES,YES	ENCSR173USI
SIN3B	ENCFF526SSV,ENCFF953OIR	ENCFF044ZTQ,ENCFF119GEQ	-phred33,-phred33	YES,YES	ENCSR632PAG
SMARCA4	ENCFF218VUS,ENCFF890WRL	ENCFF281JRR,ENCFF538IMN	-phred33,-phred33	YES,YES	ENCSR173USI
SMARCA5	ENCFF444TNL,ENCFF247XZL	ENCFF387THV,ENCFF975TEJ	-phred33,-phred33	YES,YES	ENCSR173USI
SMARCE1	ENCFF034ESF,ENCFF668WFT	ENCFF615EVJ,ENCFF531SYI	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
SOX6	ENCFF002DPJ,ENCFF002EGF	ENCFF002DUA,ENCFF002DPM	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
SP1	ENCFF002DPL,ENCFF002EGC	ENCFF002EGO,ENCFF002DPP	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
SUPT5H	ENCFF086VLI,ENCFF397FZD	ENCFF383EJJ,ENCFF077SPQ	-phred33,-phred33	YES,YES	ENCSR173USI
TAL1	ENCFF998YDA,ENCFF768SQO	ENCFF793WFY,ENCFF145TXO	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
TARDBP	ENCFF996BVX,ENCFF654EXQ	ENCFF922VLO,ENCFF228EWB	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
TCF12	ENCFF920GYM,ENCFF171QIU	ENCFF017IMJ,ENCFF349MIY	-phred33,-phred33	YES,YES	ENCSR173USI
THRAP3	ENCFF484TAP,ENCFF251TJX	ENCFF814WTW,ENCFF975CDD	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR632PAG
TRIM24	ENCFF983NMP,ENCFF150KRT	ENCFF112ZYY,ENCFF567LMF	-phred33,-phred33	YES,YES	ENCSR173USI
YBX1	ENCFF909OVD,ENCFF079PYC	ENCFF011GGX,ENCFF183DJQ	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
YBX3	ENCFF276AYD,ENCFF519SRC	ENCFF590TAH,ENCFF504AIH	-phred33,-phred33	YES,YES	ENCSR173USI
ZBTB2	ENCFF796XXQ,ENCFF920GKJ	ENCFF223BRR,ENCFF839NSQ	-phred33,-phred33	YES,YES	ENCSR173USI
ZBTB33	ENCFF094FKH,ENCFF968SHS	ENCFF332XKO,ENCFF189UUN	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ZBTB40	ENCFF520UHF,ENCFF687MII	ENCFF673KNX,ENCFF297TMP	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ZEB2	ENCFF776VEC,ENCFF824ADC	ENCFF845FIV,ENCFF349FIQ	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ZNF24	ENCFF546WQO,ENCFF294CYY	ENCFF931UCZ,ENCFF965QBT	-phred33,-phred33	YES,YES	ENCSR173USI_ENCSR554FDZ
ZNF316	ENCFF387RWU,ENCFF107ZQT	ENCFF228VAR,ENCFF739VNU	-phred33,-phred33	YES,YES	ENCSR173USI
ZNF318	ENCFF610TGH,ENCFF976NWF	ENCFF187NIJ,ENCFF252HPP	-phred33,-phred33	YES,YES	ENCSR632PAG
ZNF407	ENCFF864KXI,ENCFF678SZB	ENCFF379LAP,ENCFF253FCU	-phred33,-phred33	YES,YES	ENCSR173USI
ZNF592	ENCFF706KLP,ENCFF669OSV	ENCFF283GNM,ENCFF451RCX	-phred33,-phred33	YES,YES	ENCSR632PAG
ZSCAN29	ENCFF959OBE,ENCFF846BUV	ENCFF368UTN,ENCFF025FSC	-phred33,-phred33	YES,YES	ENCSR173USI
ZZZ3	ENCFF233HVB,ENCFF778PUP	ENCFF960PEL,ENCFF491EOP	-phred33,-phred33	YES,YES	ENCSR632PAG
