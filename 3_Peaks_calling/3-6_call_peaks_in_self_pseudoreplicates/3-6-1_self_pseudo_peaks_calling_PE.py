import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self_pseudo/self_pseudo_peaks_calling_log_PE.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	control = line_db[5]

	inputcontrol = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment_control/"+control+"/"+control+"_pooled_for_macs2.tagAlign.gz"

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self_pseudo/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	for i in range(2):
		for j in range(2):
			inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/STAR_alignment/"+tf+"/"+tf+"_rep"+str(i+1)+"_for_macs2.pr"+str(j+1)+".tagAlign.gz"
			run_macs2 = "macs2 callpeak -t "+inputfile+" -c "+inputcontrol+" --outdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/self_pseudo/"+tf+"/ -f BEDPE -n "+tf+"_rep"+str(i+1)+"_pr"+str(j+1)+"_vs_control --keep-dup auto -g hs -p 1e-3 --to-large"
			print(run_macs2)
			log.write(run_macs2+"\n\n")
			macs2 = subprocess.Popen(run_macs2, shell=True)
			macs2.communicate()

ref.close()
log.close()