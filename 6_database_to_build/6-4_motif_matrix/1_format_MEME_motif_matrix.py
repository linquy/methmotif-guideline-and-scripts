all_list = open("list_of_tf.txt","r")

cell_line ="IMR-90"

for line in all_list:
	tf = line.strip("\n").split("\t")[0]
	matrix = open(cell_line+"_motif_matrix/MM1_HSA_"+cell_line+"_"+tf+"-motif-MEME.txt","r")
	matrix_new = ""
	count = 1
	for matrix_line in matrix:
		matrix_new = matrix_new + matrix_line
		count = count+1
		if count == 10:
			break
	matrix_new = matrix_new + "MOTIF MM1_HSA_"+cell_line+"_"+tf+" "+tf+"\n"
	x = next(matrix)
	for matrix_line in matrix:
		matrix_new = matrix_new + matrix_line
	new_matrix_out = open(cell_line+"_motif_matrix_new/MM1_HSA_"+cell_line+"_"+tf+"-motif-MEME.txt","w")
	new_matrix_out.write(matrix_new)
	matrix.close()
	new_matrix_out.close()

all_list.close()