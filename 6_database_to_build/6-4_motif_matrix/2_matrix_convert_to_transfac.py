import subprocess

list_of_tf = open("list_of_tf.txt","r")
log = open("convert_matrix_to_TRANSFAC_log.txt","w")

cell_line = "IMR-90"

for tf in list_of_tf:
	tf = tf.strip("\n").strip()
	tf = tf.replace("eGFP-","")
	input_file = cell_line+"_motif_matrix_new/MM1_HSA_"+cell_line+"_"+tf+"-motif-MEME.txt"
	output_file = "MM1_HSA_"+cell_line+"_"+tf+"-motif-TRANSFAC-raw.txt"
	run_matrix_convert = "convert-matrix  -from meme -to transfac -i "+input_file+" -pseudo 1 -multiply 1 -decimals 1 -perm 0 -bg_pseudo 0.01 -return counts -to transfac -o "+output_file
	print(run_matrix_convert)
	log.write(run_matrix_convert+"\n")
	matrix_convert = subprocess.Popen(run_matrix_convert, shell=True)
	matrix_convert.communicate()
list_of_tf.close()
log.close()