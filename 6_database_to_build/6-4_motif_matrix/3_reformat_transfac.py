list_of_tf = open("list_of_tf.txt","r")
cell_line = "IMR-90"

for tf in list_of_tf:
	tf = tf.strip("\n").strip()
	tf = tf.replace("eGFP-","")
	raw = open("MM1_HSA_"+cell_line+"_"+tf+"-motif-TRANSFAC-raw.txt", "r")
	for line in raw:
		if line.find("sites:")>=0:
			line_db = line.strip("\n").split(" ")
			line_db_element = list(filter(lambda x: x != '', line_db))
			nsite = line_db_element[-1]
			nsite = 1.0*float(nsite)

	raw.close()


	raw = open("MM1_HSA_"+cell_line+"_"+tf+"-motif-TRANSFAC-raw.txt", "r")
	reformat = open(cell_line+"_motif_matrix_new/MM1_HSA_"+cell_line+"_"+tf+"-motif-TRANSFAC.txt", "w")

	writeout = "AC MM1_HSA_"+cell_line+"_"+tf+"\nXX\nID "+tf+"\nXX\nDE MM1_HSA_"+cell_line+"_"+tf+" "+tf+" ; from MethMotif\nPO\tA\tC\tG\tT\n"
	for line in raw:
		line_db = line.strip("\n").split(" ")
		if line_db[0].isdigit() == True:
			line_db_4_num = list(filter(lambda x: x != '', line_db))
			new_writeout = line_db_4_num[0]
			for i in range(1,len(line_db_4_num)):
				new_writeout = new_writeout + "\t" + str(round(float(line_db_4_num[i])*nsite))
			writeout = writeout + new_writeout + "\n"
	writeout = writeout + "XX\nCC program: MethMotif\nXX\n//\n"
	reformat.write(writeout)
	raw.close()
	reformat.close()

