import subprocess

tf_list = open("idr_analysis_judge.txt","r")
log = open("count_peak_num_and_CG_num_for_all_factors_log.txt","w")
result = open("IMR-90count_peak_num_and_CG_num_for_all_factors.txt","w")

cell_line = "IMR-90"
download_data = "Mar-26-2018"


for tf_item in tf_list:
	tf_db = (tf_item.strip("\n").strip().split("\t"))
	tf = tf_db[0]
	tf_name = tf.replace("eGFP-","")
	out_self = int(tf_db[1])
	out_pooled = int(tf_db[2])
	out_self1 = tf_db[4].split(",")[0]
	out_self2 = tf_db[4].split(",")[1]
	judge_initial = tf_db[5]
	if judge_initial == "good":
		judge = "Good"
	if judge_initial == "bad":
		judge = "Disqualified"

	if tf.find("GFP")>=0:
		remark = "eGFP"
	else:
		remark = "None"

	#count peak num 
	peak_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/Peak_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peaks_no_blacklist.narrowPeak"
	run_count_peak_num = "cat "+peak_file+" | wc -l"
	print(run_count_peak_num)
	log.write(run_count_peak_num+"\n\n")
	count_peak_num = subprocess.Popen(run_count_peak_num, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_peak_num,err_peak_num = count_peak_num.communicate()
	out_peak_num = (out_peak_num.decode("utf-8")).strip("\n").strip()

	#count CG num
	inputfile_methyl = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/methylation_profile/"+tf+"/"+tf+"_methylation_profile_in_peak_summit_200bp_raw_intersection_5cov.txt"
	run_methyl = "cut -f 1,2,3 "+inputfile_methyl+" | sort -k1,1 -k2,12n | wc -l"
	print(run_methyl)
	count_methyl = subprocess.Popen(run_methyl, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_methyl, err_methyl = count_methyl.communicate()
	out_methyl = (out_methyl.decode("utf-8")).strip("\n").strip()

	writeinput = cell_line+","+tf_name+","+download_data+","+str(out_peak_num)+","+str(out_self)+","+str(out_pooled)+","+str(out_self1)+","+str(out_self2)+","+str(out_methyl)+","+judge+","+remark+"\n"
	result.write(writeinput)

tf_list.close()
log.close()
result.close()


