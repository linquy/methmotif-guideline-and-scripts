import subprocess

tf_table = open("Identified_TF_list.txt","r")
list_out = open("IMR-90_motif_sequence_coordinates.txt","w")

for tf_line in tf_table:
	tf_db = tf_line.strip("\n").split("\t")
	tf = tf_db[0]

	if tf_db[3] == "YES":
		output_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/motif_sequence_coordinates/"+tf.replace("eGFP-","")+"_final_motif_sets.txt"
		input_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/final_motif_sets.txt"

		run_mv = "cp "+input_file+" "+output_file
		mv = subprocess.Popen(run_mv, shell=True)
		mv.communicate()

		writeout = tf.replace("eGFP-","")+","+tf.replace("eGFP-","")+"_final_motif_sets.txt\n"
		list_out.write(writeout)

tf_table.close()
list_out.close()