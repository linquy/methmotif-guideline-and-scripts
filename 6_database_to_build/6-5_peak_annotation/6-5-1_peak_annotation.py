import subprocess

list_of_tf = open("list_of_TF.txt", "r")

for tf in list_of_tf:
	tf = tf.strip("\n")

	print(tf)

	peak_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/Peak_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peak_summit_no_blacklist.bed"
	motif_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/final_motif_sets.txt"
	peak_with_motif_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/peak_with_motif/"+tf+"_peaks_with_motif.txt"
	run_count_peak_with_motif = '''intersectBed -a <(awk 'BEGIN{OFS="\t"}{print $1,$2-100,$3+100,$4}' '''+peak_file+") -b "+motif_file+''' -wa | sort -k 4,4 | uniq | awk 'BEGIN{OFS="\t"}{print $1,$2+100,$3-100,$4}' > ''' +peak_with_motif_file
	print(run_count_peak_with_motif)
	count_peak_with_motif = subprocess.Popen(run_count_peak_with_motif, shell=True, executable='/bin/bash')
	count_peak_with_motif.communicate()

	outputfile = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/Peak_annotation_of_"+tf+"_binding_sites.txt"
	run_peak_annotation = "annotatePeaks.pl "+peak_with_motif_file+" hg38 > "+outputfile
	print(run_peak_annotation)
	peak_annotation = subprocess.Popen(run_peak_annotation, shell=True)
	peak_annotation.communicate()

	stat_out_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/statistics_of_peak_annotation_of_"+tf+"_binding_sites.txt"
	stat_out = open(stat_out_file, "w")

	num_3utr = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /3\\x27/' | wc -l"
	count_3utr = subprocess.Popen(num_3utr, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_3utr, err_3utr = count_3utr.communicate()
	out_3utr = (out_3utr.decode("utf-8")).strip("\n").strip()
	stat_out.write("3\' UTR:\t"+out_3utr+"\n")
	
	num_5utr = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /5\\x27/' | wc -l"
	count_5utr = subprocess.Popen(num_5utr, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_5utr, err_5utr = count_5utr.communicate()
	out_5utr = (out_5utr.decode("utf-8")).strip("\n").strip()
	stat_out.write("5\' UTR:\t"+out_5utr+"\n")

	num_exon = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /exon/' | wc -l"
	count_exon = subprocess.Popen(num_exon, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_exon, err_exon = count_exon.communicate()
	out_exon = (out_exon.decode("utf-8")).strip("\n").strip()
	stat_out.write("exon:\t"+out_exon+"\n")

	num_intergenic = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /Intergenic/' | wc -l"
	count_intergenic = subprocess.Popen(num_intergenic, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_intergenic, err_intergenic = count_intergenic.communicate()
	out_intergenic = (out_intergenic.decode("utf-8")).strip("\n").strip()
	stat_out.write("intergenic:\t"+out_intergenic+"\n")

	num_intron = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /intron/' | wc -l"
	count_intron = subprocess.Popen(num_intron, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_intron, err_intron = count_intron.communicate()
	out_intron = (out_intron.decode("utf-8")).strip("\n").strip()
	stat_out.write("intron:\t"+out_intron+"\n")

	num_noncoding = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /non-coding/' | wc -l"
	count_noncoding = subprocess.Popen(num_noncoding, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_noncoding, err_noncoding = count_noncoding.communicate()
	out_noncoding = (out_noncoding.decode("utf-8")).strip("\n").strip()
	stat_out.write("non-coding:\t"+out_noncoding+"\n")

	num_promoter = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /promoter/' | wc -l"
	count_promoter = subprocess.Popen(num_promoter, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_promoter, err_promoter = count_promoter.communicate()
	out_promoter = (out_promoter.decode("utf-8")).strip("\n").strip()
	stat_out.write("promoter:\t"+out_promoter+"\n")

	num_TTS = "awk 'NR>1' "+outputfile+" | cut -f 8 | cut -d ' ' -f 1 | awk '$1 ~ /TTS/' | wc -l"
	count_TTS = subprocess.Popen(num_TTS, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out_TTS, err_TTS = count_TTS.communicate()
	out_TTS = (out_TTS.decode("utf-8")).strip("\n").strip()
	stat_out.write("TTS:\t"+out_TTS+"\n")

	stat_out.close()

	run_pie_chart = "Rscript peak_annotation_pie_chart.R "+tf+" "+out_3utr+" "+out_5utr+" "+out_exon+" "+out_intergenic+" "+out_intron+" "+out_noncoding+" "+out_promoter+" "+out_TTS
	pie_chart = subprocess.Popen(run_pie_chart, shell=True)
	pie_chart.communicate()

list_of_tf.close()


