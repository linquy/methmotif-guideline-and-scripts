import subprocess

cell_line = "IMR-90"
ID_base = "MM1_HSA_IMR-90_"
list_of_tf = open("list_of_TF.txt", "r")
piechart_out = open("list_name_of_piechart.txt","a+")
stat_out = open("list_name_of_stat.txt", "a+")


for tf in list_of_tf:
	tf = tf.strip("\n")

	if tf.find("eGFP-")>=0:
		tf_new = tf.split("-")[1]
	else:
		tf_new = tf
		
	ID = ID_base+tf_new
	

	piechart = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/"+tf+"_PieChart_of_peak_annotations.png"
	piechart_new_name = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/"+ID+"-TFBS-annotation-PieChart.png"
	run_cp = "cp "+piechart+" "+piechart_new_name
	cp = subprocess.Popen(run_cp, shell=True)
	cp.communicate()
	piechart_out.write(cell_line+"\t"+tf_new+"\t"+ID+"-TFBS-annotation-PieChart.png\n")

	stat_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/statistics_of_peak_annotation_of_"+tf+"_binding_sites.txt"
	stat_file_new_name = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/peak_annotation/"+ID+"-TFBS-annotation.txt"
	run_cp = "cp "+stat_file+" "+stat_file_new_name
	cp = subprocess.Popen(run_cp, shell=True)
	cp.communicate()
	stat_out.write(cell_line+"\t"+tf_new+"\t"+ID+"-TFBS-annotation.txt\n")

list_of_tf.close()
piechart_out.close()
stat_out.close()
