import subprocess
import os

tf_table = open("Identified_TF_list.txt","r")
log = open("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/methylation_score/extract_methylation_score_for_CG_in_motif_log.txt","w")

for tf_line in tf_table:
	tf_db = tf_line.strip("\n").split("\t")
	tf = tf_db[0]

	if tf_db[3] == "YES":
		#calculate motif length
		motif_seq_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/motif_sequence.txt"
		motif_flines = open(motif_seq_file,"r")
		motif_example = (next(motif_flines)).strip("\n")
		motif_len = str(len(motif_example))
		motif_flines.close()

		#meth info for each site
		run_Rscript_methinfo = "Rscript meth_info_for_each_site.R "+tf+" "+motif_len
		print(run_Rscript_methinfo)
		log.write(run_Rscript_methinfo+"\n\n")
		Rscript_methinfo = subprocess.Popen(run_Rscript_methinfo, shell=True)
		Rscript_methinfo.communicate()

tf_table.close()
log.close()






