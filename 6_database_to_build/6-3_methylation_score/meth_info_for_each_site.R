#parse bash command
args = commandArgs()
tf = args[6]
motif_len = as.integer(args[7])

#read methylation file
file_name = paste("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/",tf,"/methylation_profile_for_CGs_in_motifs_5cov.txt", sep = "")
meth = tryCatch(read.table(file_name,sep = "\t"), error=function(e) data.frame())
empty_matrix = FALSE


if(nrow(meth)==0){
  empty_matrix = TRUE
} else{
  meth_d = meth[which(meth$V10=="+"),]
  meth_r = meth[which(meth$V10=="-"),]
  
  meth_d$dis = meth_d$V2-meth_d$V8+1
  meth_r$dis = motif_len-(meth_r$V2-meth_r$V8)
  
  if(nrow(meth_d)>0){
    for (i in 1:nrow(meth_d)){
      if (unlist(strsplit(as.character(meth_d[i,14]), split=""))[as.integer(meth_d[i,15])]=="G"){
        meth_d[i,15] = meth_d[i,15]-1
      }
    }
  }
  
  if(nrow(meth_r)>0){
    for (i in 1:nrow(meth_r)){
      if (unlist(strsplit(as.character(meth_r[i,14]), split=""))[as.integer(meth_r[i,15])]=="G"){
        meth_r[i,15] = meth_r[i,15]-1
      }
    }
  }
  
  if(nrow(meth_d)>0){
    meth_d$id = paste(meth_d$V7,meth_d$V8,meth_d$V9,meth_d$dis,sep = "")
    meth_d_sub = data.frame()
    meth_d_id_uniq = unique(meth_d$id)
    for (i in meth_d_id_uniq){
      meth_d_temp = meth_d[which(meth_d$id==i),c("V5","V6","dis")]
      dis_temp = meth_d_temp[1,3]
      meth_temp = 100*sum(meth_d_temp[,1])/sum(meth_d_temp[,c(1,2)])
      new_add = data.frame(i,dis_temp,meth_temp)
      meth_d_sub = rbind(meth_d_sub, new_add)
    }
  }
  
  if(nrow(meth_r)>0){
    meth_r$id = paste(meth_r$V7,meth_r$V8,meth_r$V9,meth_r$dis,sep = "")
    meth_r_sub = data.frame()
    meth_r_id_uniq = unique(meth_r$id)
    for (i in meth_r_id_uniq){
      meth_r_temp = meth_r[which(meth_r$id==i),c("V5","V6","dis")]
      dis_temp = meth_r_temp[1,3]
      meth_temp = 100*sum(meth_r_temp[,1])/sum(meth_r_temp[,c(1,2)])
      new_add = data.frame(i,dis_temp,meth_temp)
      meth_r_sub = rbind(meth_r_sub, new_add)
    }
  }
  
  
  if(nrow(meth_d)==0 && nrow(meth_r)==0){
    empty_matrix = TRUE
  } else if(nrow(meth_d)==0 && nrow(meth_r)!=0){
    meth_sub = meth_r_sub[,2:3]
    colnames(meth_sub) = c("dis","meth")
  } else if(nrow(meth_d)!=0 && nrow(meth_r)==0){
    meth_sub = meth_d_sub[,2:3]
    colnames(meth_sub) = c("dis","meth")
  } else if(nrow(meth_d)!=0 && nrow(meth_r)!=0){
    meth_sub = rbind(meth_d_sub[,2:3], meth_r_sub[,2:3])
    colnames(meth_sub) = c("dis","meth")
  }
}



if(empty_matrix==TRUE){
  plot_matrix = data.frame(matrix(0,nrow = 3, ncol = motif_len))
  colnames(plot_matrix) = c(seq(1,motif_len,1))
} else{
  plot_matrix = data.frame(c(1,2,3))
  for (i in seq(1,motif_len,1)){
    meth_sub_i = meth_sub[which(meth_sub$dis == i),]
    unmeth_count= nrow(meth_sub_i[which(meth_sub_i$meth<10),])
    meth_count= nrow(meth_sub_i[which(meth_sub_i$meth>90),])
    inbetween_count = nrow(meth_sub_i)-unmeth_count-meth_count
    new_column = c(unmeth_count,inbetween_count,meth_count)
    plot_matrix = data.frame(plot_matrix,new_column)
    
  }
  plot_matrix = plot_matrix[,2:ncol(plot_matrix)]
  colnames(plot_matrix) = c(seq(1,motif_len,1))
}

plot_matrix$position = c("beta score < 10","beta score in between","beta score > 90")
plot_matrix_new = plot_matrix[,c(motif_len+1,1:motif_len)]


tf_name = gsub("eGFP-", "", tf)
writeout = paste("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/methylation_score/MM1_HSA_IMR-90_",tf_name,"-methScore.txt",sep = "")
write.table(plot_matrix_new,writeout, sep = "\t", col.names = T, row.names = F, quote = F)




