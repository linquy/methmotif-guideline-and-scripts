import subprocess

tf_table = open("Identified_tfs_for_motif_analysis_rescan.txt","r")
log = open("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/TFs_need_to_be_re-scanned_by_FIMO_with_MEME_motif_Log.txt","w")

for tf_line in tf_table:
	tf_line_db = tf_line.strip("\n").split("\t")
	tf = tf_line_db[0]
	motif = tf_line_db[4]

	background = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/meme_result/background"
	outdir = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/re-scan_by_fimo"
	input_xml = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/meme_result/meme_out/meme.xml"
	input_fasta = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/"+tf+"_optimal_peak_summit_no_blacklist_200bp.fasta"

	run_fimo = "fimo --parse-genomic-coord --oc "+outdir+" --bgfile "+background+" --motif "+motif+" "+input_xml+" "+input_fasta
	print(run_fimo)
	log.write(run_fimo+"\n\n")
	fimo = subprocess.Popen(run_fimo, shell=True)
	fimo.communicate()

tf_table.close()
log.close()
