import subprocess

tf_table = open("Identified_TF_list.txt","r")
list_out = open("IMR-90_motif_sequence_coordinates_with_meth_info.txt","w")

for tf_line in tf_table:
	tf_db = tf_line.strip("\n").split("\t")

	tf = tf_db[0]
	if tf_db[3] == "YES":
		output_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/motif_sequence_coordinates_with_meth_info/"+tf.replace("eGFP-","")+"_methylation_profile_for_CGs_in_motifs.txt"
		input_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/methylation_profile_for_CGs_in_motifs_5cov.txt"

		run_mv = "cp "+input_file+" "+output_file
		mv = subprocess.Popen(run_mv, shell=True)
		mv.communicate()

		writeout = tf.replace("eGFP-","")+","+tf.replace("eGFP-","")+"_methylation_profile_for_CGs_in_motifs.txt\n"
		list_out.write(writeout)

tf_table.close()
list_out.close()