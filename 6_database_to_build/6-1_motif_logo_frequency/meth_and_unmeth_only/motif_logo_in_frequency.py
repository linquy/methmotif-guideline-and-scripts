import subprocess

tf_table = open("list_of_TF.txt","r")
log = open("/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/motif_logo_frequency/methylated_only/motif_logo_log.txt","w")

for tf_line in tf_table:
	tf = tf_line.strip("\n")

	motif_seq_file = "/home/quy/extra_container/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/motif_sequence.txt"
	motif_flines = open(motif_seq_file,"r")
	motif_example = (next(motif_flines)).strip("\n")
	motif_len = str(len(motif_example))
	motif_flines.close()


	#meth info for each site
	run_Rscript_methinfo = "Rscript meth_info_for_each_site.R "+tf+" "+motif_len
	print(run_Rscript_methinfo)
	log.write(run_Rscript_methinfo+"\n\n")
	Rscript_methinfo = subprocess.Popen(run_Rscript_methinfo, shell=True)
	Rscript_methinfo.communicate()

tf_table.close()
log.close()