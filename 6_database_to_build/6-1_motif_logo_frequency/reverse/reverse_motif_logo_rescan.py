import subprocess

tf_table = open("Identified_tfs_for_motif_analysis_rescan.txt","r")
log = open("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/database_to_build/motif_logo_frequency_rev/reverse_motif_logo_Log_rescan.txt","w")

for tf_line in tf_table:
	tf_db = tf_line.strip("\n").split("\t")

	tf = tf_db[0]
	if tf_db[3] == "YES":
		# reverse motif seq
		run_Rscript_reverse_seq = "Rscript reverse_motif_sequence.R "+tf
		print(run_Rscript_reverse_seq)
		log.write(run_Rscript_reverse_seq+"\n\n")
		Rscript_reverse_seq = subprocess.Popen(run_Rscript_reverse_seq, shell=True)
		Rscript_reverse_seq.communicate()


		#generate reverse motif logo
		motif_seq_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/motif_sequence_reverse.txt"
		weblogo_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/IMR-90_"+tf+"_motif_logo_reverse.eps"
		run_generate_weblogo = "weblogo -F eps --errorbars NO  --fineprint '' -X NO -Y NO -c classic -U probability -f "+motif_seq_file+" -o "+weblogo_file
		print(run_generate_weblogo)
		log.write(run_generate_weblogo+"\n\n")
		generate_weblogo = subprocess.check_call(["/bin/bash", "-c", run_generate_weblogo])

		#calculate motif length
		motif_flines = open(motif_seq_file,"r")
		motif_example = (next(motif_flines)).strip("\n")
		motif_len = str(len(motif_example))
		motif_flines.close()


		#meth info for each site
		run_Rscript_methinfo = "Rscript meth_info_for_each_site_rev.R "+tf+" "+motif_len
		print(run_Rscript_methinfo)
		log.write(run_Rscript_methinfo+"\n\n")
		Rscript_methinfo = subprocess.Popen(run_Rscript_methinfo, shell=True)
		Rscript_methinfo.communicate()

tf_table.close()
log.close()
