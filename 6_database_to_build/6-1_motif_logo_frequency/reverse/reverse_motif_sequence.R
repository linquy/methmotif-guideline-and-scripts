#parse bash command
args = commandArgs()
tf = args[6]

file_name = paste("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/", tf, "/motif_sequence.txt", sep = "")
motif_seq = read.table(file_name,sep = "\t")

motif_seq$new_seq = chartr("ATGC","TACG",motif_seq$V1)

new_motif = data.frame()

for (i in 1:nrow(motif_seq)){
  new_seq = motif_seq[i,c("new_seq")]
  new_seq_rev = paste(rev(substring(new_seq,1:nchar(new_seq),1:nchar(new_seq))),collapse="")
  new_add =data.frame(new_seq_rev)
  colnames(new_add) = "new_seq_rev"
  new_motif = rbind(new_motif,new_add)
}

writeout_file_name = paste("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/",tf,"/motif_sequence_reverse.txt", sep = "")

write.table(new_motif, writeout_file_name, sep = "\t", row.names = F, col.names = F, quote = F)

if (file.exists(writeout_file_name)){
  print("done with reverse")
}
