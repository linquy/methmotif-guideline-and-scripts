import subprocess
import os

tf_table = open("Identified_tfs_for_motif_analysis_no_rescan.txt","r")
log = open("/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/call_meth_info_at_TF_motif_log_no_scan.txt","w")

for tf_line in tf_table:
	tf_line_db = tf_line.strip("\n").split("\t")
	tf = tf_line_db[0]
	enrich_judge = tf_line_db[3]
	file_subnum = tf_line_db[4]

	if enrich_judge == "YES":

		#mkdir
		run_mkdir = "mkdir /root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf
		mkdir = subprocess.Popen(run_mkdir, shell=True)
		mkdir.communicate()

		# final motif sets
		input_raw_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/"+tf+"/meme_result/fimo_out_"+file_subnum+"/fimo.txt"
		motif_set = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/final_motif_sets.txt"
		run_final_motif_set = '''awk 'NR>1' '''+input_raw_file+''' | cut -d "=" -f 2 > '''+motif_set
		print(run_final_motif_set)
		log.write(run_final_motif_set+"\n\n")
		final_motif_set = subprocess.check_call(["/bin/bash", "-c", run_final_motif_set])

		#check motif set
		if os.stat(motif_set).st_size == 0:
			run_rm = "rm -r /root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf
			print("zero motif set found and start removing files")
			log.write("zero motif set found and start removing files\n\n")
			print(run_rm)
			log.write(run_rm+"\n\n")
			rm = subprocess.Popen(run_rm, shell=True)
			rm.communicate()
			continue

		#call meth info at motif sets
		wgbs_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/WGBS/ENCFF937OSM.bismark.cov.gz"
		meth_info_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/methylation_profile_for_CGs_in_motifs.txt"
		run_meth_calling = "intersectBed -a <(zcat "+wgbs_file+") -b "+motif_set+" -wa -wb > "+meth_info_file
		print(run_meth_calling)
		log.write(run_meth_calling+"\n\n")
		meth_calling = subprocess.check_call(["/bin/bash", "-c", run_meth_calling])

		#enough coverage in WGBS
		meth_info_file_3cov = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/methylation_profile_for_CGs_in_motifs_5cov.txt"
		run_enough_coverage = "awk '$5+$6>=5' "+meth_info_file+" > "+meth_info_file_3cov
		print(run_enough_coverage)
		log.write(run_enough_coverage+"\n\n")
		enough_coverage = subprocess.check_call(["/bin/bash", "-c", run_enough_coverage])

		# produce motif sequence
		motif_seq_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/motif_sequence.txt"
		run_produce_motif_seq = "cut -f 8 "+motif_set+" > "+motif_seq_file
		print(run_produce_motif_seq)
		log.write(run_produce_motif_seq+"\n\n")
		produce_motif_seq = subprocess.Popen(run_produce_motif_seq, shell=True)
		produce_motif_seq.communicate()

		#generate weblogo
		weblogo_file = "/root/IMR-90_methylation_profiles_at_TF_peaks/transcription_factors/motif_analysis/final_results/"+tf+"/IMR-90_"+tf+"_motif_logo.eps"
		run_generate_weblogo = "weblogo -F eps --errorbars NO  --fineprint '' -X NO -Y NO -c classic -U probability -f "+motif_seq_file+" -o "+weblogo_file
		print(run_generate_weblogo)
		log.write(run_generate_weblogo+"\n\n")
		generate_weblogo = subprocess.check_call(["/bin/bash", "-c", run_generate_weblogo])

		#calculate motif length
		motif_flines = open(motif_seq_file,"r")
		motif_example = (next(motif_flines)).strip("\n")
		motif_len = str(len(motif_example))
		motif_flines.close()

		#meth info for each site
		run_Rscript_methinfo = "Rscript meth_info_for_each_site.R "+tf+" "+motif_len
		print(run_Rscript_methinfo)
		log.write(run_Rscript_methinfo+"\n\n")
		Rscript_methinfo = subprocess.Popen(run_Rscript_methinfo, shell=True)
		Rscript_methinfo.communicate()

tf_table.close()
log.close()






