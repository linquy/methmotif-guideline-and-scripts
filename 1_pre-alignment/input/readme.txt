PE - paired end
SE - single end

control_list_SE_template.txt is separated by tab and columns represent ENCODE experiment accession, ENCODE accessions for ChIPseq replicate, phred (based on FASTQC results), and judegement if ChIPseq quality is good or not (based on FASTQC results).

control_list_PE_template.txt is separated by tab and columns represent ENCODE experiment accession, ENCODE accessions for two ChIPseq read ends in replicate one, and ENCODE accessions for two ChIPseq read ends in replicate two.

command line: python3 fastqc_PE.py
command line: python3 fastqc_SE.py

Based on Fastqc results, proceed to read trimming:
command line: python3 trimmomatic_SE.py
