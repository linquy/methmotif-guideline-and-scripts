import subprocess

ref = open("control_list_PE_template.txt","r")
out = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/fastqc_PE_log.txt","w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep1_db = line_db[1].split(",")
	rep2_db = line_db[2].split(",")

	for db_i in rep1_db:
		run_fastqc = "fastqc -t 10 /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"+db_i+".fastq.gz -o /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"
		print(run_fastqc)
		out.write(run_fastqc+"\n\n")
		fastqc = subprocess.Popen(run_fastqc, shell=True)
		fastqc.communicate()

	for db_i in rep2_db:
		run_fastqc = "fastqc -t 10 /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"+db_i+".fastq.gz -o /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"
		print(run_fastqc)
		out.write(run_fastqc+"\n\n")
		fastqc = subprocess.Popen(run_fastqc, shell=True)
		fastqc.communicate()


ref.close()
out.close()
