import subprocess

ref = open("control_list_SE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/Trimmomatic_SE_log.txt", "w")

for line in ref:
	line_db = line.strip("\n").split("\t")
	file = line_db[0]
	rep_db = line_db[1].split(",")
	form_db = line_db[2].split(",")
	judge_db = line_db[3].split(",")
	for i in range(len(judge_db)):
		if judge_db[i] == "NO":
			form = form_db[i]
			rep = rep_db[i]

			run_trim = "java -jar ~/analysis_tools/Trimmomatic-0.36/trimmomatic-0.36.jar SE -threads 10 "+form+" /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep+".fastq.gz /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep+"_trimmed.fastq.gz  SLIDINGWINDOW:4:20 MINLEN:28"
			print(run_trim)
			log.write(run_trim+"\n")
			trim = subprocess.Popen(run_trim, shell=True)
			trim.communicate()

			run_fastqc = "fastqc -t 10 /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"+rep+"_trimmed.fastq.gz -o /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control/"+file+"/"
			print(run_fastqc)
			log.write(run_fastqc+"\n\n")
			fastqc = subprocess.Popen(run_fastqc, shell=True)
			fastqc.communicate()

ref.close()
log.close()