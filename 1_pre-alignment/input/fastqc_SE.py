import subprocess

ref = open("control_list_SE_template.txt","r")
out = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/fastqc_SE_log.txt","w")


for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	rep_db = line_db[1].split(",")
	for rep in rep_db:
		run_fastqc = "fastqc -t 10 /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"+rep+".fastq.gz -o /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data_control_files_archived/"+tf+"/"
		print(run_fastqc)
		out.write(run_fastqc+"\n\n")
		fastqc = subprocess.Popen(run_fastqc, shell=True)
		fastqc.communicate()


ref.close()
out.close()