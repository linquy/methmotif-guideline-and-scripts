import subprocess

ref = open("TFs_with_their_replicates_PE_template.txt", "r")

for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]
	for i in range(len(line_db)-1):
		sub_db = line_db[i+1].split(",")
		for sub in sub_db:
			run_fastqc = "fastqc -t 10 -o /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/" + tf +"/rep"+str(i+1)+"/   /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/raw_data/" + tf + "/rep"+str(i+1)+"/" + sub + ".fastq.gz"
			print(run_fastqc)
			fastqc = subprocess.Popen(run_fastqc, shell = True)
			fastqc.communicate() 

ref.close()