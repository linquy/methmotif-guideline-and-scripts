import subprocess

ref = open("TFs_with_their_replicates_SE_template.txt", "r")
log = open("/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/methylation_profile/methylation_profile_at_summit_200bp_SE_log.txt","w")


for line in ref:
	line_db = line.strip("\n").split("\t")
	tf = line_db[0]

	run_mkdir = "mkdir /home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/methylation_profile/"+tf
	mkdir = subprocess.Popen(run_mkdir, shell=True)
	mkdir.communicate()

	inputfile = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/Peaks_calling/idr_analysis/final_set/"+tf+"/"+tf+"_optimal_peak_summit.bed"
	wgbs = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/K562_WGBS_pooled_copy/pooled_replicates_with_enought_coverage_CpG_report_with_methScore.txt"
	output = "/home/quy/back_up_for_other_container/K562_methylation_profiles_at_TF_peaks/transcription_factors_updated_on_16JUN2017/methylation_profile/"+tf+"/"+tf+"_methylation_profile_in_peak_summit_200bp_raw_intersection.txt"
	run_intersect = "intersectBed -a "+wgbs+" -b <(awk 'BEGIN{FS=\"\t\";OFS=\"\t\"}{print $1,$2-100,$3+100,$4,$5}' "+inputfile+") -wa -wb > "+output
	print(run_intersect)
	log.write(run_intersect+"\n\n")
	intersect = subprocess.check_call(["/bin/bash", "-c", run_intersect])

ref.close()
log.close()

